import app from 'firebase/app';
import "firebase/functions";
import "firebase/firebase-firestore";

const config = require("./config.json");

class Firebase {
  constructor() {
      app.initializeApp(config);
      this.db = app.firestore();
      this.functions = app.functions();
  }

  get = (collection, id) =>{
    if (id !== undefined){
      return this.db.collection(collection).doc(id).get();
    }
    
    return this.db.collection(collection).get();
  }  

  sendEmail = ({email, displayName})=> {
    const sendEmail = this.functions.httpsCallable('sendWelcomeEmail');

    return new Promise(async (resolve, reject)=>{
      try{
          const res = await sendEmail({email, displayName});
          console.log(res);
          /*if (res.data.errorInfo){
              const { code } = res.data.errorInfo;
              
              reject('Hubo un error en la creación de éste usuario, por favor revisa los parámetros', code);
          } else{
              resolve(res.data);
          }*/
      } catch(e){
          reject(e);
      }
    });
  }
}

  export default new Firebase();