import React from 'react';
import './App.css';

import Loading from './loading.gif';

function View({isLoading = true, data, dates, members}) {
  let names = ["", ""];
  
  try{
    names = members[dates.next].nombre.split(" ");
  } catch{

  }

  if (isLoading){
    return (
      <div className="App">
        <img src={Loading} alt="loading" />
      </div>
    )
  }

  return (
    <div className={"App" + (data.isOutOfDate ? ' is-out-of-date' : '')}>
        <div className="clock">
          <div className="part">
            <h1>{data.days}</h1>
            <label>Día{(data.days === '01' ? '' : 's')}</label>
          </div>
          <div className="part">
            <h1>{data.hours}</h1>
            <label>Hora{(data.hours === '01' ? '' : 's')}</label>
          </div>
          <div className="part">
            <h1>{data.minutes}</h1>
            <label>Minuto{(data.minutes === '01' ? '' : 's')}</label>
          </div>
          <div className="part">
            <h1>{data.seconds}</h1>
            <label>Segundo{(data.seconds === '01' ? '' : 's')}</label>
          </div>
        </div>
        {data.isOutOfDate === false && (
          <React.Fragment>
            <p>para el siguiente commit</p>
            <h2 className="name">{(members[dates.next] || {nombre : ""}).nombre}</h2>
            <p className="owner-label">es el responsable</p>
          </React.Fragment>
        )}

        {(data.isOutOfDate) && (
          <p>lleva <span className="name">{names[0]}</span> haciéndose el pendejo</p>
        )}
        {members[1] !== undefined && (
          <React.Fragment>
            <div className="next-container">
              <p>Luego Sigue</p>
              <h3 className="next">{members[(dates.following || 1)].nombre}</h3>
            </div>
          </React.Fragment>
        )}
    </div>
  );
}

export default View;
