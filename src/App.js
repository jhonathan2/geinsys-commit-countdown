import React from 'react';
import moment from "moment";

import Firebase from "./firebase";

import View from './View';

function App() {
  const [data, setData] = React.useState({});
  const [dates, setDates] = React.useState({});
  const [members, setMembers] = React.useState({});
  const [isLoading, setLoadingStatus] = React.useState(true);

  React.useEffect(()=>{
    let remaining_days = 0;

    while (moment().add(remaining_days, 'days').format('ddd') !== 'Thu'){
      remaining_days++;
    }

    const _dates = {};
    const _members = {};
    

    Firebase.get('owner')
    .then((lastOwners)=>{

      lastOwners.forEach((_lo)=>{
        _dates[_lo.id] = _lo.data().value;
      });

      let membersCounter = 0;
      Firebase.get('integrantes')
      .then((data)=>{
        data.forEach((record)=>{
          const _data = record.data();
          _members[_data.orden] = _data;
          membersCounter++;
        })

        Firebase.sendEmail({email : "jhonathanespinosa@geinsys.com", displayName : 'Jotachito'})
        .then((response)=>{
          console.log(response);
        })
        .catch((error)=>{
          console.log(error);
        })

        setMembers(_members);
  
        if (_dates.next === membersCounter){
          _dates.following = 1;
        } else{
          _dates.following = _dates.next + 1;
        }

        setDates(_dates);
    
        tickFunction({ setData, remaining_days });

        setLoadingStatus(false);
      });
    });
  }, []);


  return (
    <View data={data} dates={dates} members={members} isLoading={isLoading}/>
  );
}

export default App;

const tickFunction = ({ setData, remaining_days, difference = 0 }) => {
  const finalDate = moment().hour(16).minute(0).second(0).add(remaining_days, 'days');
      
  const 
    __difference = finalDate.diff(moment(), 'seconds'),
    isOutOfDate = __difference < 0,
    _difference = Math.abs(__difference);
  
  const 
    _days = Math.floor(_difference / ( 60 * 60 * 24)),
    _hours = Math.floor((_difference % ( 60 * 60 * 24)) / ( 60 * 60)),
    _minutes = Math.floor((_difference % ( 60 * 60)) / ( 60)),
    _seconds = Math.floor((_difference % (60))),
    _data = {};
  
  _data.days = (_days > 9 ? _days : `0${_days}`);
  _data.hours = (_hours > 9 ? _hours : `0${_hours}`);
  _data.minutes = (_minutes > 9 ? _minutes : `0${_minutes}`);
  _data.seconds = (_seconds > 9 ? _seconds : `0${_seconds}`);
  _data.isOutOfDate = isOutOfDate;
  
  if ((isOutOfDate && difference < _difference) || (!isOutOfDate && difference > _difference)){
    setData({ ..._data });
  }

  setTimeout(()=>{
    tickFunction({ setData, remaining_days, difference : _difference});
  }, 500)
}